#! /bin/sh

HOME=/home/pi
DOSENET=$HOME/dosenet-raspberrypi

LOG=/tmp/sender_manager.log

os_info=$(cat /etc/os-release)

VERSION_ID=$(echo "$os_info" | grep -oP 'VERSION_ID="\K[^"]+')

if [ "$VERSION_ID" -gt "10" ]; then
  PYTHON_PATH=$HOME/dosenet/bin/python
else
  PYTHON_PATH=python
fi

case "$1" in
  start)
    echo "Starting sender script"  > $LOG
    sudo $PYTHON_PATH $DOSENET/managers.py --sender --log --logfile $LOG >>$LOG 2>&1
    echo "Finished sender script"  >> $LOG
    ;;
  test)
    echo "Starting sender script in test mode" > $LOG
    sudo $PYTHON_PATH $DOSENET/managers.py --sender --test --log --logfile $LOG >> $LOG 2>&1
    ;;
  stop)
    echo "Stopping sender script" >> $LOG
    sudo pkill -SIGQUIT -f managers.py
    ;;
 *)
    echo "Usage: /home/pi/dosenet-raspberrypi/sender.sh {start|test|stop}"
    exit 1
    ;;
esac

exit 0
