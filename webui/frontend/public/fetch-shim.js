// This fetch shim automatically appends the JWT token
// to the request headers. Removing this file will prevent
// the WebUI from properly interfacing with the backend.

const oldFetch = window.fetch

window.fetch = function (url, init) {
  const jwt = localStorage.getItem('access_token')
  if (jwt) {
    init = init ?? {}
    init.headers = {
      ...(init.headers ?? {}),
      Authorization: `Bearer ${jwt}`,
    }
  }
  return oldFetch(url, init)
}
