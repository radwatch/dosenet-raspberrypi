import { HOST, mapping } from '@/constants'
import { ChangeEvent, FormEvent, useState } from 'react'

export default function Home() {
  const [formData, setFormData] = useState({
    name: '',
    nick: '',
    lat: '',
    long: '',
    display: false,
    pocket: false,
    d3s: false,
    AQ: false,
    CO2: false,
    weather: false,
    password: '',
  })

  const [message, setMessage] = useState('')
  const [submitting, setSubmitting] = useState(false)

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value, type, checked } = event.target

    if (type == 'checkbox') {
      setFormData((prevFormData) => ({ ...prevFormData, [name]: checked }))
    } else {
      setFormData((prevFormData) => ({ ...prevFormData, [name]: value }))
    }
  }

  const handleSubmit = (e: FormEvent) => {
    const SENSORS = ['pocket', 'd3s', 'AQ', 'CO2', 'weather']
    e.preventDefault()
    setMessage('Configuring...')
    const form = new FormData()
    for (const [key, value] of Object.entries(formData)) {
      if (SENSORS.includes(key)) continue
      if (key == 'display') form.append(key, value ? '1' : '0')
      else {
        form.append(key, value.toString())
      }
    }
    let selected_sensors = ''
    for (const sensor of SENSORS) {
      selected_sensors += formData[sensor as keyof typeof formData] ? 1 : 0
    }
    form.append('sensors', selected_sensors)

    console.log(form)
    fetch(`${HOST}/configure`, {
      method: 'POST',
      body: form,
      // headers: {
      //   'content-type': 'multipart/form-data'
      // }
    })
      .then((response) => response.json())
      .then((success) => {
        setMessage((prevMessage) => prevMessage + '\n' + success.message)
        setSubmitting(false)
      })
      .catch((error) => {
        setMessage((prevMessage) => prevMessage + '\n' + error.message)
        setSubmitting(false)
      })
  }

  return (
    <div>
      <main className="mt-24 container mx-auto px-12">
        <div className="text-xl font-bold mb-4">Configuration</div>
        <form className="" onSubmit={handleSubmit}>
          <div className="mb-5">
            <label className="block mb-2 text-sm font-medium ">
              Location Name
            </label>
            <input
              value={formData.name}
              onChange={handleChange}
              type="text"
              name="name"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
              required
            />
          </div>
          <div className="mb-5">
            <label className="block mb-2 text-sm font-medium ">Nickname</label>
            <input
              value={formData.nick}
              onChange={handleChange}
              type="text"
              name="nick"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
              required
            />
          </div>
          <div className="flex justify-between">
            <div className="mb-5 w-1/2 mr-2">
              <label className="block mb-2 text-sm font-medium ">
                Latitude
              </label>
              <input
                value={formData.lat}
                onChange={handleChange}
                type="number"
                name="lat"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                required
              />
            </div>
            <div className="mb-5 w-1/2 ml-2">
              <label className="block mb-2 text-sm font-medium ">
                Longitude
              </label>
              <input
                value={formData.long}
                onChange={handleChange}
                type="number"
                name="long"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                required
              />
            </div>
          </div>

          <div className="mb-3 flex items-center content-center">
            <label className="mr-2">Display Data?</label>
            <input
              type="checkbox"
              checked={formData.display}
              name="display"
              onChange={handleChange}
            />
          </div>

          <div className="font-bold mb-2">Sensor Select</div>
          <div className="grid mb-3">
            <div className="mb-3 inline-flex items-center content-center">
              <label className="mr-2">{mapping.pocket}</label>
              <input
                type="checkbox"
                checked={formData.pocket}
                name="pocket"
                onChange={handleChange}
              />
            </div>
            <div className="mb-3 inline-flex items-center content-center">
              <label className="mr-2">{mapping.d3s}</label>
              <input
                type="checkbox"
                checked={formData.d3s}
                name="d3s"
                onChange={handleChange}
              />
            </div>
            <div className="mb-3 inline-flex items-center content-center">
              <label className="mr-2">{mapping.AQ}</label>
              <input
                type="checkbox"
                checked={formData.AQ}
                name="AQ"
                onChange={handleChange}
              />
            </div>
            <div className="mb-3 inline-flex items-center content-center">
              <label className="mr-2">{mapping.CO2}</label>
              <input
                type="checkbox"
                checked={formData.CO2}
                name="CO2"
                onChange={handleChange}
              />
            </div>

            <div className="mb-3 inline-flex items-center content-center">
              <label className="mr-2">{mapping.weather}</label>
              <input
                type="checkbox"
                checked={formData.weather}
                name="weather"
                onChange={handleChange}
              />
            </div>
          </div>

          {/* <div className="mb-5">
            <label className="block mb-2 text-sm font-medium text-gray-900">
              Configuration file
            </label>
            <input
              className="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-gray-50"
              name="cfg"
              type="file"
              onChange={handleChange}
            />
            <p className="mt-1 text-sm text-gray-500">
              Select the provided <code>config.csv</code> file.
            </p>
          </div> */}
          <div className="mb-5">
            <label className="block mb-2 text-sm font-medium ">
              Server Password
            </label>
            <input
              value={formData.password}
              onChange={handleChange}
              type="password"
              name="password"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
              required
            />
          </div>
          <button
            type="submit"
            disabled={submitting}
            className="text-white bg-[#464646] hover:bg-gray-800 focus:ring-4 focus:outline-none font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center disabled:bg-gray-400"
          >
            Submit
          </button>
          <pre className="mt-4 text-sm text-gray-500">
            {!!message && message}
          </pre>
        </form>
      </main>
    </div>
  )
}
