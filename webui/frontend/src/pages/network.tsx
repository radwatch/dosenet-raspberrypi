import { HOST } from '@/constants'
import { ChangeEvent, FormEvent, useEffect, useState } from 'react'

const authTypes = ['NONE', 'WPA-PSK', 'WPA-EAP']

function boolToString(val: boolean) {
  return val ? 'Yes ✅' : 'No ❌'
}

export default function Network() {
  const [status, setStatus] = useState<any>()
  const [message, setMessage] = useState('')
  const [isSubmitting, setSubmitting] = useState(false)
  const [wifiForm, setWifiForm] = useState({
    ssid: '',
    key_mgmt: authTypes[0],
    psk: '',
    identity: '',
    password: '',
  })

  const handleSubmit = (e: FormEvent) => {
    e.preventDefault()
    setMessage('Configuring...')
    setSubmitting(true)
    const form = new FormData()
    for (const [key, value] of Object.entries(wifiForm)) {
      if (value) {
        form.append(key, value)
      }
    }
    if (wifiForm.key_mgmt == 'WPA-EAP') {
      form.append('eap', 'PEAP')
      form.append('phase2', 'autheap=MSCHAPV2')
    }
    console.log(form)
    fetch(`${HOST}/wifi_config`, {
      method: 'POST',
      body: form,
    })
      .then((response) => response.json())
      .then((success) => {
        setMessage(success.message)
        setSubmitting(false)
      })
      .catch((error) => {
        setMessage(error.message)
        setSubmitting(false)
      })
  }

  function handleChange(e: ChangeEvent<HTMLSelectElement | HTMLInputElement>) {
    const { name, value } = e.target
    setWifiForm((prevFormData) => ({ ...prevFormData, [name]: value }))
  }

  useEffect(() => {
    const intervalId = setInterval(() => {
      fetch(`${HOST}/wifi_status`)
        .then((response) => response.json())
        .then((data) => {
          setStatus(data)
        })
    }, 5000)

    return () => clearInterval(intervalId)
  }, [])

  return (
    <div>
      <main className="mt-24 container mx-auto px-12 pb-12">
        <div className="font-bold text-xl mb-2">Status</div>

        <div className="bg-gray-50 rounded border border-gray-300 mb-1 p-2">
          {status ? (
            <>
              <div className="flex">
                <div className="mr-1">Internet Connectivity:</div>
                <div className="">{boolToString(status.connectivity)}</div>
              </div>
              <div className="flex">
                <div className="mr-1">Ethernet Link:</div>
                <div className="">{boolToString(status.eth_link)}</div>
              </div>
              <hr className="border border-1 rounded border-gray-800 my-2" />
              <div className="flex flex-col">
                <div className="mr-1">WiFi Configuration:</div>
                <pre className="bg-gray-300 rounded p-2 my-2">
                  {status.wifi_config}
                </pre>
              </div>
              <div className="flex">
                <div className="mr-1">WiFi State:</div>
                <div className="italic">{status.wifi_status.wpa_state}</div>
              </div>
            </>
          ) : (
            'Loading...'
          )}
        </div>
        <div className="text-gray-600 italic text-sm mb-5">
          Updates every 5 seconds
        </div>

        <div className="text-xl font-bold mb-4">Configure WiFi</div>
        <div className="mb-4">The WebUI's network setup wizard is very basic and may not support your institution's networking configuration. If you are unable to connect to your wireless access point, try using the webshell instead (you may need to contact your IT administrator/help desk).</div>
        <form className="" onSubmit={handleSubmit}>
          <div className="mb-5">
            <label className="block mb-2 text-sm font-medium ">SSID</label>
            <input
              type="text"
              value={wifiForm.ssid}
              onChange={handleChange}
              name="ssid"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
              required
            />
          </div>
          <div className="flex items-center content-center flex-row mb-2">
            <div className="mr-1 text-sm">Authentication: </div>
            <select
              value={wifiForm.key_mgmt}
              name="key_mgmt"
              onChange={handleChange}
              className="p-1 rounded bg-gray-50 border-gray-300 border text-sm"
            >
              {authTypes.map((method) => (
                <option key={method} value={method}>
                  {method}
                </option>
              ))}
            </select>
          </div>
          {wifiForm.key_mgmt == 'WPA-PSK' && (
            <div className="mb-5">
              <label className="block mb-2 text-sm font-medium ">
                Password
              </label>
              <input
                type="password"
                value={wifiForm.psk}
                onChange={handleChange}
                name="psk"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                required
              />
            </div>
          )}
          {wifiForm.key_mgmt == 'WPA-EAP' && (
            <>
              <div className="mb-5">
                <label className="block mb-2 text-sm font-medium ">
                  Identity
                </label>
                <input
                  type="text"
                  name="identity"
                  value={wifiForm.identity}
                  onChange={handleChange}
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                  required
                />
              </div>
              <div className="mb-1">
                <label className="block mb-2 text-sm font-medium ">
                  Password
                </label>
                <input
                  type="password"
                  name="password"
                  value={wifiForm.password}
                  onChange={handleChange}
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                  required
                />
              </div>
              <div className="text-sm text-gray-500 mb-2">
                Assuming PEAP and MSCHAPv2
              </div>
            </>
          )}
          <button
            type="submit"
            disabled={isSubmitting}
            className="text-white bg-[#464646] hover:bg-gray-800 focus:ring-4 focus:outline-none font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center disabled:bg-gray-400"
          >
            Add/Modify Connection
          </button>
        </form>
        <div>{message}</div>
      </main>
    </div>
  )
}
