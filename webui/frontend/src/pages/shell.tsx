
import { HOST } from '@/constants';
import dynamic from 'next/dynamic';
import { useState } from 'react';

const WebShell = dynamic(() => import('./components/WebShell'), { ssr: false });

export default function Shell() {
    const [acceptedConfirmation, setAcceptedConfirmation] = useState(false)

    return <div className="container mt-24 mx-auto px-12 pb-12">
        <div className="text-xl font-bold mb-4">Shell</div>
        <div className="text-sm mb-4">The web shell is only meant for use by advanced users. If you are not sure what you are doing, please do not use this feature.</div>

        {acceptedConfirmation
            ? <WebShell host={HOST.replace(/http/i, "ws")} route="/webshell" />
            : <button onClick={() => setAcceptedConfirmation(true)} className="text-white bg-[#464646] hover:bg-gray-800 focus:ring-4 focus:outline-none font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center disabled:bg-gray-400">Proceed</button>}
    </div>
}