import { ChangeEvent, useEffect, useState } from 'react'
import { HOST, mapping } from '@/constants'

export default function Data() {
  const [log, setLog] = useState<any>()
  const [sensor, setSensor] = useState('pocket')

  function handleChange(e: ChangeEvent<HTMLSelectElement>) {
    setSensor(e.target.value)
  }

  useEffect(() => {
    setTimeout(() => {
      fetch(`${HOST}/get_log?sensor=${sensor}`)
        .then((response) => response.json())
        .then((success) => setLog(success.message))
        .catch((error) => setLog(error.message))
    }, 500)
  }, [sensor])

  return (
    <div>
      <main className="mt-24 container mx-auto px-12">
        <div className="text-xl font-bold mb-4">Data</div>
        <div className="flex items-center content-center flex-row mb-2">
          <div className="mr-1">Select Sensor: </div>
          <select
            value={sensor}
            onChange={handleChange}
            className="p-1 rounded bg-gray-50 border-gray-300 border"
          >
            {Object.entries(mapping).map(([key, value]) => (
              <option value={key}>{value}</option>
            ))}
          </select>
        </div>
        {log && (
          <p className="text-gray-600 mb-2 text-sm">
            Displaying last 100 lines
          </p>
        )}
        <div className="bg-gray-50 rounded border border-gray-300 mb-5">
          <pre className="p-2">
            {!log && 'Loading...'}
            {log && log}
          </pre>
        </div>
      </main>
    </div>
  )
}
