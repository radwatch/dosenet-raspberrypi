import { HOST } from "@/constants"
import { use, useEffect, useState } from "react"

export default function Reboot() {
    const [isWorking, setIsWorking] = useState(true)
    const [showError, setShowError] = useState(false)
    const [showSuccess, setShowSuccess] = useState(false)
    const [successDetails, setSuccessDetails] = useState('')
    const [error, setError] = useState('')
    const [apEnabled, setApEnabled] = useState(false)
    const [form, setForm] = useState({
        username: "",
        password: "",
        passwordConfirm: ""
    });

    const handleChange = (e: { target: { name: any; value: any } }) => {
        const { name, value } = e.target;
        setForm((prevState) => ({ ...prevState, [name]: value }));
    };

    const handleKeyPress = (e: React.KeyboardEvent<HTMLInputElement>) => {
        if (e.key === "Enter") {
            changeCredentials();
        }
    };

    function changeCredentials() {
        setIsWorking(true)
        setShowError(false)
        setShowSuccess(false)
        setError('')

        if (form.username.trim() === "" || form.password.trim() === "") {
            setIsWorking(false)
            setError("Please fill in all fields!")
            setShowError(true)
            return
        } else if (form.password !== form.passwordConfirm) {
            setIsWorking(false)
            setError("Passwords do not match!")
            setShowError(true)
            return
        }

        fetch(`${HOST}/auth/change`, {
            method: 'POST',
            body: JSON.stringify({
                username: form.username,
                password: form.password
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(async (response) => {
                const res = await response.json()
                if (response.ok) {
                    setIsWorking(false)
                    setShowError(false)
                    setError('')
                    setSuccessDetails("Successfully changed credentials!")
                    setShowSuccess(true)
                } else {
                    setIsWorking(false)
                    setError(res.message)
                    setShowError(true)
                }
            })
            .catch((error) => {
                setIsWorking(false)
                setError(error.stack ?? error.message)
                setShowError(true)
            })
    }


    function reboot() {
        setIsWorking(true)
        setShowError(false)
        setShowSuccess(false)
        setError('')
        fetch(`${HOST}/reboot`, {
            method: 'POST',
        })
            .then(async (response) => {
                const res = await response.json()
                if (response.ok) {
                    setIsWorking(false)
                    setShowError(false)
                    setError('')
                    setSuccessDetails("Reboot command successfully issued! The reboot process may take a few minutes to complete.")
                    setShowSuccess(true)
                } else {
                    setIsWorking(false)
                    setError(res.message)
                    setShowError(true)
                }
            })
            .catch((error) => {
                setIsWorking(false)
                setError(error.stack ?? error.message)
                setShowError(true)
            })
    }

    function eject() {
        setIsWorking(true)
        setShowError(false)
        setShowSuccess(false)
        setError('')
        fetch(`${HOST}/eject`, {
            method: 'POST',
        })
            .then(async (response) => {
                const res = await response.json()
                if (response.ok) {
                    setIsWorking(false)
                    setShowError(false)
                    setError('')
                    setSuccessDetails("Eject command successfully issued! The eject process may take a few minutes to complete.")
                    setShowSuccess(true)
                } else {
                    setIsWorking(false)
                    setError(res.message)
                    setShowError(true)
                }
            })
            .catch((error) => {
                setIsWorking(false)
                setError(error.stack ?? error.message)
                setShowError(true)
            })
    }

    function logOut() {
        window.localStorage.removeItem('access_token')
        window.localStorage.removeItem('expiration')
        window.location.href = "/login"
    }

    function toggleAp() {
        setIsWorking(true)
        setShowError(false)
        setShowSuccess(false)
        setError('')

        fetch(`${HOST}/set_ap`, {
            method: 'POST',
            body: JSON.stringify({}),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(async (response) => {
                const res = await response.json()
                if (response.ok) {
                    setIsWorking(false)
                    setShowError(false)
                    setError('')
                    setSuccessDetails(`The access point is now ${res.new_state}!`)
                    setApEnabled(res.state)
                    setShowSuccess(true)
                } else {
                    setIsWorking(false)
                    setError(res.message)
                    setShowError(true)
                }
            })
            .catch((error) => {
                setIsWorking(false)
                setError(error.stack ?? error.message)
                setShowError(true)
            })
    }

    useEffect(() => {
        fetch(`${HOST}/get_ap`)
            .then(async response => {
                const status: { status: boolean } = await response.json()
                setApEnabled(status.status)
                setIsWorking(false)
            })
    }, [])

    return <div className="mt-24 container mx-auto px-12">
        {showError && <div className="w-full h-50 bg-[#e88181] outline-none rounded-md p-3 mb-5 ring-[#a82424] ring-2">
            <b className="text-xl font-bold mb-4">Error</b>
            <div>Something went wrong whilst issuing the command; please try again later. Follows is the error in question: <br></br> {error}</div>
        </div>}
        {showSuccess && <div className="w-full h-50 bg-[#83e881] outline-none rounded-md p-3 mb-5 ring-[#47a824] ring-2">
            <b className="text-xl font-bold mb-4">Success</b>
            <div>{successDetails}</div>
        </div>}
        <div className="text-2xl font-bold mb-4">Danger Zone</div>
        <div className="mb-4">All actions here may carry unwanted consequences, and can potentially lock yourself out of this device. It is recommended that you exert your utmost caution while on this page.</div>

        <div className="text-xl font-bold mb-4">{apEnabled ? "Disable Access Point" : "Enable Access Point"}</div>
        <div className="mb-4">{apEnabled ? "Disable" : "Enable"} the Access Point on this DoseNet device. This option should generally be used after setup is finished. An Internet connection is generally required to disable the AP.
            <br></br>The access point is currently <b>{apEnabled ? "ENABLED" : "DISABLED"}</b>.
        </div>
        <button
            onClick={toggleAp}
            disabled={isWorking}
            className="mb-4 text-white bg-[#464646] hover:bg-gray-800 focus:ring-4 focus:outline-none font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center disabled:bg-gray-400"
        >{apEnabled ? "Disable AP" : "Enable AP"}</button>

        <div className="text-xl font-bold mb-4">Reboot</div>
        <div className="mb-4"><b>WARNING!</b> Pressing the button below will trigger a system-wide reboot. </div>
        <button
            onClick={reboot}
            disabled={isWorking}
            className="text-white bg-[#464646] hover:bg-gray-800 focus:ring-4 focus:outline-none font-medium rounded-lg text-sm w-full mb-4 sm:w-auto px-5 py-2.5 text-center disabled:bg-gray-400"
        >Reboot</button>

        <div className="text-xl font-bold mb-4">Eject</div>
        <div className="mb-4"><b>WARNING!</b> Pressing the button below will eject the web interface from this DoseNet device, permanently removing it. Further administrative actions must be taken via SSH.</div>
        <button
            onClick={eject}
            disabled={isWorking}
            className="mb-4 text-white bg-[#464646] hover:bg-gray-800 focus:ring-4 focus:outline-none font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center disabled:bg-gray-400"
        >Eject</button>

        <div className="text-xl font-bold mb-4">Change Credentials</div>
        <div className="mb-4"><b>WARNING!</b> Changing your access credentials may lock you out of the WebUI! You have been warned.</div>
        <label className="text-sm mb-4">New username:</label>
        <input
            type="text"
            name="username"
            className="mb-4 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
            value={form.username}
            onChange={handleChange}
            onKeyPress={handleKeyPress}
            disabled={isWorking}
        />
        <label className="text-sm mb-4">New password:</label>
        <input
            type="password"
            name="password"
            className="mb-4 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
            value={form.password}
            onChange={handleChange}
            onKeyPress={handleKeyPress}
            disabled={isWorking}
        />
        <label className="text-sm mb-4">Confirm new password:</label>
        <input
            type="password"
            name="passwordConfirm"
            className="mb-4 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
            value={form.passwordConfirm}
            onChange={handleChange}
            onKeyPress={handleKeyPress}
            disabled={isWorking}
        />
        <button
            onClick={changeCredentials}
            disabled={isWorking}
            className="mb-4 text-white bg-[#464646] hover:bg-gray-800 focus:ring-4 focus:outline-none font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center disabled:bg-gray-400"
        >
            Change Login Credentials
        </button>

        <div className="text-xl font-bold mb-4">Log Out </div>
        <div className="mb-4">Log out of the WebUI.</div>
        <button
            onClick={logOut}
            disabled={isWorking}
            className="mb-4 text-white bg-[#464646] hover:bg-gray-800 focus:ring-4 focus:outline-none font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center disabled:bg-gray-400"
        >Log Out</button>
    </div>
}