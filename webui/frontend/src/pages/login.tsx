import { HOST } from "@/constants";
import { useEffect, useState } from "react";

export default function LogIn() {
    const [showError, setShowError] = useState(false);
    const [error, setError] = useState("");
    const [allowed, setAllowed] = useState(true);
    const [form, setForm] = useState({
        username: "",
        password: ""
    });

    const handleChange = (e: { target: { name: any; value: any } }) => {
        const { name, value } = e.target;
        setForm((prevState) => ({ ...prevState, [name]: value }));
    };

    const handleKeyPress = (e: React.KeyboardEvent<HTMLInputElement>) => {
        if (e.key === "Enter") {
            authenticate();
        }
    };

    async function authenticate() {
        if (form.username.trim() === "" || form.password.trim() === "") {
            setError("Please fill in all fields!");
            setShowError(true);
            return;
        } else if (!allowed) return;

        setAllowed(false);
        setShowError(false);

        await fetch(`${HOST}/auth`, { credentials: "include", method: "POST", body: JSON.stringify(form), headers: { "Content-Type": "application/json" } })
            .then(async (response) => {
                setError("");
                if (response.status === 200) {
                    const json: { access_token: string, expires_in: string } = await response.json();
                    localStorage.setItem("access_token", json.access_token);

                    // parse for expiration time
                    localStorage.setItem("expiration", json.expires_in);

                    window.location.href = "/";
                } else {
                    setAllowed(true);
                    setError(`Invalid login credentials; please ensure that your username and password are correct. (${response.status} ${response.statusText})`);
                    setShowError(true);
                }
            })
            .catch((error) => {
                setError(error.stack ?? error.message);
                setShowError(true);
                setAllowed(true);
            });
    }

    useEffect(() => {
        const expiration = localStorage.getItem('expiration')
        if (expiration && Date.now() < parseInt(expiration) * 1000) {
            window.location.href = "/"
        }
    }, [])

    return (
        <div className="container mt-24 mx-auto px-12 pb-12">
            <div className="text-2xl font-bold mb-4">Please Log In</div>
            <div className="text-sm mb-4">
                Authentication is required to access the device. Please enter your
                username and password below to proceed.
            </div>
            {showError && (
                <div className="w-full h-50 bg-[#e88181] outline-none rounded-md p-3 mb-5 ring-[#a82424] ring-2">
                    <b className="text-xl font-bold mb-4">Error</b>
                    <div>{error}</div>
                </div>
            )}

            <label className="text-sm mb-4">Username:</label>
            <input
                type="text"
                name="username"
                className="mb-4 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                value={form.username}
                onChange={handleChange}
                onKeyPress={handleKeyPress}
                disabled={!allowed}
            />
            <label className="text-sm mb-4">Password:</label>
            <input
                type="password"
                name="password"
                className="mb-4 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                value={form.password}
                onChange={handleChange}
                onKeyPress={handleKeyPress}
                disabled={!allowed}
            />
            <button
                onClick={authenticate}
                disabled={!allowed}
                className="text-white bg-[#464646] hover:bg-gray-800 focus:ring-4 focus:outline-none font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center disabled:bg-gray-400"
            >
                Log In
            </button>
        </div>
    );
}