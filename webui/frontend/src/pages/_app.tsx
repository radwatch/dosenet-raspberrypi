import '@/style.css'
import type { AppProps } from 'next/app'
import Layout from './components/Layout'
import Script from 'next/script'

export default function App({ Component, pageProps }: AppProps) {
  return (
    <Layout>
      <Script src="/fetch-shim.js"></Script>
      <title>DoseNet WebUI</title>
      <Component {...pageProps} />
    </Layout>
  )
}
