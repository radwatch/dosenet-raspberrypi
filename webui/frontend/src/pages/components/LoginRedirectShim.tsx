"use client"

import { HOST } from "@/constants"
import { useEffect } from "react"

export default function LoginRedirectShim() {
    useEffect(() => {
        function update() {
            const token = localStorage.getItem('access_token'),
                expiration = localStorage.getItem('expiration')

            if (new URL(window.location.href).pathname !== '/login') {
                if (!token || !expiration) {
                    window.location.href = '/login'
                } else if (Date.now() >= parseInt(expiration)) {
                    localStorage.removeItem('access_token')
                    localStorage.removeItem('expiration')
                    window.location.href = '/login'
                }
            }
        }

        async function checkToken() {
            if (new URL(window.location.href).pathname !== '/login') {
                await fetch(`${HOST}/validate_token`)
                    .then(async (response) => {
                        if (response.ok) {
                            // do nothing
                        } else {
                            localStorage.removeItem('access_token')
                            localStorage.removeItem('expiration')
                            window.location.href = '/login'
                        }
                    })
                    .catch(() => {
                        // internet down?

                        // localStorage.removeItem('access_token')
                        // localStorage.removeItem('expiration')
                        // window.location.href = '/login'
                    })
            }
        }
        update()

        const interval = setInterval(update, 1000)
        const interval2 = setInterval(checkToken, 30 * 1000)

        return () => {
            clearInterval(interval)
            clearInterval(interval2)
        }
    }, [])
    return <></>
}