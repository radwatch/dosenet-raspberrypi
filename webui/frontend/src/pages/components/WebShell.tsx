"use client";

import { HTMLAttributes, useEffect, useRef } from "react";
import { HOST } from "@/constants";

import "@xterm/xterm/css/xterm.css";
import { Manager, Socket } from "socket.io-client";
import { DefaultEventsMap } from "@socket.io/component-emitter";

import { Terminal } from "@xterm/xterm";
// import { CanvasAddon } from "@xterm/addon-canvas";
// import { ClipboardAddon } from "@xterm/addon-clipboard";
// import { FitAddon } from "@xterm/addon-fit";

export default function WebShell({ route, host }: { route: string, host: string }) {
    const ref = useRef<HTMLDivElement | null>(null)

    useEffect(() => {
        let handleResize: () => any, terminal: Terminal, sock: Socket<DefaultEventsMap, DefaultEventsMap>, handleUnload = () => {
            if (terminal) {
                terminal.dispose()
                window.removeEventListener('resize', handleResize)
            } else {
                let interval = setInterval(() => {
                    if (terminal) {
                        terminal.dispose()
                        window.removeEventListener('resize', handleResize)
                        clearInterval(interval)
                    }
                }, 60)
            }
            if (sock) sock.disconnect()
            else {
                let interval = setInterval(() => {
                    if (sock) {
                        sock.disconnect()
                        clearInterval(interval)
                    }
                }, 60)
            }
        }

        (async () => {
            const { Terminal } = await import("@xterm/xterm")
            const { FitAddon } = await import("@xterm/addon-fit")
            const { ClipboardAddon } = await import("@xterm/addon-clipboard")
            const { CanvasAddon } = await import("@xterm/addon-canvas")

            terminal = new Terminal()
            terminal.open(ref.current!)
            terminal.loadAddon(new FitAddon())
            terminal.loadAddon(new ClipboardAddon())
            terminal.loadAddon(new CanvasAddon())

            const fitAddon = new FitAddon()
            terminal.loadAddon(fitAddon)

            handleResize = () => {
                fitAddon.fit()
                sock.emit('resize', { cols: terminal.cols, rows: terminal.rows })
            }
            window.addEventListener('resize', handleResize)
            setTimeout(() => {
                fitAddon.fit()
                terminal.focus()
            }, 0)

            // websocket stuff
            const manager = new Manager(host)
            sock = manager.socket(route)

            fetch(`${HOST}/webshell/token`)
                .then(async res => {
                    const response: { token: string } = await res.json()
                    sock.emit("login", response)
                })

            sock.on('output', (data: string) => {
                terminal.write(data)
            })

            terminal.onData(data => {
                sock.emit("input", data)
            })

            sock.on('disconnect', () => {
                terminal.writeln("[disconnected]")
                handleResize()
            })

            sock.on('login_success', () => {
                terminal.writeln("[server accepted our token; logged in]")
                handleResize()
            })

            sock.on('exit', (data: { code: number }) => {
                terminal.writeln(`[process exited with code ${data.code}]`)
            })
        })()

        return handleUnload
    }, [ref.current])

    return <div ref={ref}></div>
}