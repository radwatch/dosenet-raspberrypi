import Link from "next/link";
import { useEffect, useState } from "react";

export default function Header() {
  const [loggedIn, setLoggedIn] = useState(false);

  useEffect(() => {
    const token = localStorage.getItem('access_token'),
      expiration = localStorage.getItem('expiration')

    if (!token || !expiration || (Date.now() >= parseInt(expiration) * 1000)) {
      setLoggedIn(false)
    } else setLoggedIn(true)
  }, [loggedIn])

  return (
    <nav className="bg-[#464646] h-12 flex items-center justify-center fixed top-0 w-full">
      <div className="flex justify-between container px-4 items-center text-white">
        <div className="text-lg font-bold">DoseNet WebUI</div>
        {loggedIn ? <div className="">
          <Link href="/" className="mx-2 hover:text-gray-400">Config</Link>
          <Link href="/network" className="mx-2 hover:text-gray-400">Network</Link>
          <Link href="/data" className="mx-2 hover:text-gray-400">Logs</Link>
          <Link href="/shell" className="mx-2 hover:text-gray-400">Shell</Link>
          <Link href="/danger" className="mx-2 hover:text-gray-400">Danger Zone</Link>
        </div> : <div className="" />}
      </div>
    </nav>
  );
}
