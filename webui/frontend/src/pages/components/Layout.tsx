
import dynamic from "next/dynamic";

const LoginRedirectShim = dynamic(() => import("./LoginRedirectShim").then(mod => mod.default), { ssr: false });
const Header = dynamic(() => import("./Header"), { ssr: false });

export default function Layout({ children }: any) {
  return (
    <>
      <Header />
      {<LoginRedirectShim />}
      <main>{children}</main>
      {/* <Footer /> */}
    </>
  )
}