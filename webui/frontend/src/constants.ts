export const mapping = {
  pocket: 'Pocket Geiger',
  d3s: 'D3S',
  AQ: 'Air Quality',
  CO2: 'CO2',
  weather: 'Weather',
}

export const HOST = 'http://localhost:5000'
