#!/usr/bin/env python3
import os


# this check is nested inside of a function to prevent pylance from malding
def check_os():
    if os.name == "nt":
        print("***** UNSUPPORTED OS!")
        print(
            "The backend server is Linux-only. More specifically, this server only runs on Raspbian."
        )
        print(
            "You may have varying degrees of success running on other Linux distributions, but it is not guaranteed."
        )
        exit(1)


check_os()

from flask import Flask, request, redirect, jsonify
from flask_socketio import SocketIO, disconnect
from werkzeug.exceptions import HTTPException
import subprocess
from http import HTTPStatus
import requests
from flask_cors import CORS, cross_origin

from flask_jwt_extended import JWTManager, jwt_required, create_access_token
from threading import Timer

from flask_limiter import Limiter
from flask_limiter.util import get_remote_address

import fcntl
import termios
import struct

from network_wrapper import *
from db import DBTool
from datetime import timedelta
import time
import dotenv
import pty
import select
import auth
import asyncio
from flask import send_from_directory

dotenv.load_dotenv()

app = Flask(__name__)

app.config["SECRET_KEY"] = os.environ.get("SECRET_KEY")

if os.environ.get("SECRET_KEY") is None:
    print("[warn] Could not find secret key in environment! Loading from creds.txt")
    app.config["SECRET_KEY"] = auth.get_jwt_secret()


cors = CORS(app, supports_credentials=True)

jwt = JWTManager(app)
socketio = SocketIO(app, cors_allowed_origins="*")
limiter = Limiter(
    get_remote_address,
    app=app,
    default_limits=["500 per minute", "2 per second"],
    default_limits_per_method=True,
    storage_uri="memory://",
)


# incredibly heavy function wtf
def cache_self_hostnames():
    global hostname_cache
    hostname_cache = get_hostnames_and_ips()
    Timer(60, cache_self_hostnames).start()


cache_self_hostnames()


@app.before_request
def redirect_to_self():
    global hostname_cache
    app_domain = "192.168.4.1"
    app_port = 80

    # Get the host from the request
    request_host = request.host.split(":")[0]

    if (
        request_host not in hostname_cache
        and not request_host == "localhost"
        and not request_host.endswith(".local")
        and not request_host.startswith("127")
    ):
        new_url = f"http://{app_domain}:{app_port}/"
        return redirect(new_url, code=301)


# host the static frontend files in the static folder
@app.route("/")
@limiter.limit("1000 per second", override_defaults=True)
def index():
    return send_from_directory("static", "index.html")


@app.route("/<path:path>")
@limiter.limit("1000 per second", override_defaults=True)
def static_proxy(path):
    # check if a file extension is present
    if "." in path.split("/")[-1]:
        return send_from_directory("static", path)
    else:
        return send_from_directory("static", path + ".html")


SENSORS = ["weather", "pocket", "sender", "d3s", "CO2", "AQ"]


@app.route("/validate_token", methods=["GET"])
@jwt_required()
@limiter.limit("1 per 10 seconds")
def validate():
    return {"ok": True}


@app.route("/auth", methods=["POST"])
@limiter.limit("10 per minute", override_defaults=True)
def login():
    username = request.json.get("username")
    password = request.json.get("password")
    if username is None or password is None:
        return {"message": "Missing username or password"}, HTTPStatus.UNAUTHORIZED
    elif auth.get_username() != username or not auth.check_password(password):
        return {"message": "Bad credentials"}, HTTPStatus.UNAUTHORIZED
    else:
        token = create_access_token(identity=username, expires_delta=timedelta(days=2))
        return {
            "access_token": token,
            # roughly 2 days
            "expires_in": round((time.time() + (2 * 24 * 60 * 60)) * 1000),
        }


@app.route("/auth/change", methods=["POST"])
def change_credentials():
    username = request.json.get("username")
    password = request.json.get("password")
    if username is None or password is None:
        return {"message": "Invalid credentials"}, HTTPStatus.BAD_REQUEST
    elif len(password) < 8:
        return {
            "message": "Password must be at least 8 characters!"
        }, HTTPStatus.BAD_REQUEST
    elif password == os.environ.get("PASSWORD"):
        return {
            "message": "Password cannot be the same as the current password!"
        }, HTTPStatus.BAD_REQUEST
    elif password == "password":
        return {
            "message": "Please choose a more secure password."
        }, HTTPStatus.BAD_REQUEST
    auth.change_credentials(username, password)
    return {"message": "Credentials changed successfully!"}


@app.errorhandler(Exception)
def handle_exception(e):
    code = 500
    if isinstance(e, HTTPException):
        code = e.code
    return jsonify(message=str(e)), code


@app.route("/configure", methods=["POST"])
@jwt_required()
def configure():
    form = request.form.to_dict()
    cpmtorem = 0.0036

    # validation
    if form["name"] is None or form["name"] == "":
        return {"message": "Name is required"}, HTTPStatus.BAD_REQUEST
    elif form["nick"] is None or form["nick"] == "":
        return {"message": "Nickname is required"}, HTTPStatus.BAD_REQUEST
    elif (
        form["lat"] is None
        or form["lat"] == ""
        or float(form["lat"]) < -90
        or float(form["lat"]) > 90
    ):
        return {
            "message": "Latitude is required and must be between -90 and 90"
        }, HTTPStatus.BAD_REQUEST
    elif (
        form["long"] is None
        or form["long"] == ""
        or float(form["long"]) < -180
        or float(form["long"]) > 180
    ):
        return {
            "message": "Longitude is required and must be between -180 and 180"
        }, HTTPStatus.BAD_REQUEST
    elif form["display"] is None or form["display"] == "":
        return {"message": "Display is required"}, HTTPStatus.BAD_REQUEST
    elif form["sensors"] is None or form["sensors"] == "":
        return {"message": "Sensors is required"}, HTTPStatus.BAD_REQUEST
    elif form["password"] is None or form["password"] == "":
        return {"message": "Password is required"}, HTTPStatus.BAD_REQUEST

    db = DBTool(
        form["name"],
        form["nick"],
        float(form["lat"]),
        float(form["long"]),
        cpmtorem,
        form["display"],
        form["sensors"],
        form["password"],
    )
    return {"message": db.output + "Configuration Succeeded"}


@app.route("/wifi_status", methods=["GET"])
@jwt_required()
def wifi():
    with open("/etc/wpa_supplicant/wpa_supplicant.conf", "r") as f:
        config = f.read()

    return {
        "wifi_config": config,
        "eth_link": eth_connected(),
        "wifi_status": wifi_status(),
        "connectivity": connectivity(),
    }


@app.route("/wifi_config", methods=["POST"])
@jwt_required()
def wifi_config():
    config = """ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=US
network={
"""

    QUOTES_REQ = ["phase2", "identity", "password", "ssid", "psk"]

    for key, value in request.form.to_dict().items():
        config += " " * 2
        if key in QUOTES_REQ:
            config += f'{key}="{value}"'
        else:
            config += f"{key}={value}"
        config += "\n"

    config += "}"

    with open("/etc/wpa_supplicant/wpa_supplicant.conf", "w") as f:
        f.write(config)

    reconfigure_wifi()

    return {"message": "Successfully configured"}


@app.route("/get_log", methods=["GET"])
@jwt_required()
def get_log():
    sensor = request.args.get("sensor")
    if sensor not in SENSORS:
        return {"message": "Invalid Sensor Selection"}, HTTPStatus.BAD_REQUEST
    filename = os.path.join("/tmp", f"{sensor}_manager.log")
    log = subprocess.check_output(["tail", "-n", "100", filename], text=True)
    return {"message": log}


@app.route("/reboot", methods=["POST"])
@jwt_required()
def reboot():
    Timer(5.0, lambda: os.system("sudo reboot"), ()).start()
    return {"message": "Restarting device..."}


@app.route("/eject", methods=["POST"])
@jwt_required()
def eject():
    Timer(5.0, eject_webui, ()).start()
    return {"message": "Ejecting WebUI..."}


@app.route("/get_ap", methods=["GET"])
@jwt_required()
@limiter.limit("10 per 5 seconds", override_defaults=True)
def get_ap():
    return {"status": is_hotspot_enabled()}


@app.route("/set_ap", methods=["POST"])
@jwt_required()
def toggle_ap():
    ap_state = is_hotspot_enabled()
    new_state = request.json.get("state", not ap_state)

    if new_state is True:
        if ap_state:
            return {"message": "Hotspot already enabled"}, HTTPStatus.BAD_REQUEST
        else:
            enable_hotspot()
            return {"message": "Hotspot enabled", "new_state": "enabled", "state": True}
    else:
        if ap_state:
            if not connectivity():
                return {
                    "message": "An Internet connection is required to disable the access point."
                }, HTTPStatus.BAD_REQUEST
            elif request.remote_addr is not None and request.remote_addr.startswith(
                "192.168.4."
            ):
                return {
                    "message": "You cannot disable the hotspot from the hotspot! You must do so through another network interface."
                }, HTTPStatus.FORBIDDEN
            disable_hotspot()
            return {
                "message": "Hotspot disabled",
                "new_state": "disabled",
                "state": False,
            }
        else:
            return {"message": "Hotspot already disabled"}, HTTPStatus.BAD_REQUEST


# webshell
sockets = {}
remove_client_map = {}
connection_tokens = {}


# this route generates an access token, which the client can use
# to create a webshell instance. the token itself is valid for 1 minute
# after creation and is required to create an instance
@app.route("/webshell/token", methods=["GET"])
@jwt_required()
@limiter.limit("5 per 10 seconds", override_defaults=True)
def get_token():
    token = os.urandom(64).hex()
    connection_tokens[token] = time.time() + 60
    return {"token": token}


def tick():
    for sid, _ in list(sockets.items()):
        poll_process(sid)
    for sid, expire_time in list(remove_client_map.items()):
        if expire_time <= time.time():
            disconnect(sid, "/webshell")
            del remove_client_map[sid]
    for token, expire_time in list(connection_tokens.items()):
        if expire_time <= time.time():
            del connection_tokens[token]
    Timer(0.001, tick).start()


def poll_process(sid):
    proc = sockets[sid]["process"]
    master_fd = sockets[sid]["master_fd"]
    if proc.poll() is not None:
        socketio.emit(
            "exit", {"code": proc.returncode}, namespace="/webshell", room=sid
        )
        disconnect(sid, "/webshell")
        del sockets[sid]
        return
    ready = select.select([master_fd], [], [], 0)[0]
    if ready:
        data = os.read(master_fd, 1024).decode()
        if data:
            socketio.emit("output", data, namespace="/webshell", room=sid)


def create_process(sid):
    master_fd, slave_fd = pty.openpty()
    proc = subprocess.Popen(
        ["/bin/bash"],
        stdin=slave_fd,
        stdout=slave_fd,
        stderr=slave_fd,
        universal_newlines=True,
        start_new_session=True,
        cwd=os.environ.get("HOME"),
    )
    sockets[sid] = {"process": proc, "master_fd": master_fd, "slave_fd": slave_fd}


@socketio.on("connect", namespace="/webshell")
def on_connect():
    remove_client_map[request.sid] = time.time() + 60


@socketio.on("disconnect", namespace="/webshell")
def on_disconnect():
    sid = request.sid
    if sid in sockets:
        proc = sockets[sid]["process"]
        proc.terminate()
        proc.wait()  # Ensure the process has terminated
        os.close(sockets[sid]["master_fd"])
        os.close(sockets[sid]["slave_fd"])
        del sockets[sid]
    elif sid in remove_client_map:
        del remove_client_map[sid]


@socketio.on("resize", namespace="/webshell")
def on_resize(data):
    sid = request.sid
    if sid not in sockets:
        if sid not in remove_client_map:
            disconnect()
    else:
        cols = data.get("cols", 80)
        rows = data.get("rows", 24)

        # perform input validation
        if not isinstance(cols, int) or cols <= 0:
            return
        if not isinstance(rows, int) or rows <= 0:
            return

        # use some crazy whizzbang to set the terminal size
        master_fd = sockets[sid]["master_fd"]
        winsize = struct.pack("HHHH", rows, cols, 0, 0)
        fcntl.ioctl(master_fd, termios.TIOCSWINSZ, winsize)
        pass


@socketio.on("login", namespace="/webshell")
def on_login(data):
    token = data.get("token")
    if token is None or token not in connection_tokens:
        disconnect()
    else:
        del connection_tokens[token]
        del remove_client_map[request.sid]
        socketio.emit("login_success", namespace="/webshell", room=request.sid)
        create_process(request.sid)


@socketio.on("input", namespace="/webshell")
def on_input(data):
    sid = request.sid
    if sid not in sockets:
        if sid not in remove_client_map:
            disconnect()
    else:
        # sockets[sid].stdin.write(data)
        # sockets[sid].stdin.flush()
        os.write(sockets[sid]["master_fd"], data.encode())


# 0.001 = 1ms
Timer(0.001, tick).start()

# if __name__ == "__main__":
#     app.run()
