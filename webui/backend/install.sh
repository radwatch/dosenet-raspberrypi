#!/bin/bash
cd $(dirname $0)

echo "Installing dependencies..."
sudo pip install gunicorn
sudo pip install -r requirements.txt
sudo apt install hostapd dnsmasq -y

echo "Reinstalling dhcpcd5..."
sudo apt remove dhcpcd5 -y 
sudo apt install dhcpd5 -y

echo "Copying configuration files..."
sudo bash -c "echo > /etc/dhcpcd.conf"
sudo bash -c "cat ./ap-config/dhcpcd.conf > /etc/dhcpcd.conf"
sudo bash -c "cat ./ap-config/dnsmasq.conf > /etc/dnsmasq.conf"

cat webui.service.template | sed "s|{DIRECTORY}|$(realpath $(dirname $0))|g" | sudo tee /etc/systemd/system/webui.service > /dev/null
sudo systemctl daemon-reload
sudo systemctl enable webui
