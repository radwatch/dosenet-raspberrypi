import subprocess
import requests
import os
import socket
import platform


def retrieve_line(string, token):
    for item in string.split("\n"):
        if token in item:
            return item.strip()


def eth_connected(interface="eth0"):
    status = subprocess.check_output(["ethtool", interface], text=True)
    if "yes" in retrieve_line(status, "Link detected"):
        return True
    else:
        return False


def wifi_status(interface="wlan0"):
    cmd_out = subprocess.check_output(["wpa_cli", "-i", interface, "status"], text=True)
    status = {}
    for line in cmd_out.splitlines():
        key, value = line.split("=")
        status[key] = value
    return status


def reconfigure_wifi(interface="wlan0"):
    output = subprocess.check_output(
        ["wpa_cli", "-i", interface, "reconfigure"], text=True
    ).strip()
    if output != "OK":
        raise Exception("WiFi Configuration Failed")


def connectivity(host="http://www.gstatic.com/generate_204"):
    try:
        req = requests.get(host)
        if req.status_code == 204:
            return True
        else:
            return False
    except Exception as e:
        return False


def is_hotspot_enabled():
    if not os.path.exists("/etc/hostapd/hostapd.conf"):
        return False
    else:
        with open("/etc/hostapd/hostapd.conf", "r") as f:
            # check if there is anything inside
            if f.read():
                return True
            else:
                return False


def enable_hotspot(ssid="RPiServerX", password="servingpiistasty!"):
    # # defaults
    # with open("/etc/default/hostapd", "rw") as f:
    #     if not retrieve_line(f.read(), 'DAEMON_CONF="/etc/hostapd/hostapd.conf"'):
    #         f.write('DAEMON_CONF="/etc/hostapd/hostapd.conf"')

    # # dhcpcd: check if config exists
    # dhcpcd = ""
    # if not os.path.exists("/etc/dhcpcd.conf"):
    #     with open("./ap-config/dhcpcd.conf", "r") as f:
    #         dhcpcd = f.read()
    #     with open("/etc/dhcpcd.conf", "w") as f:
    #         f.write(dhcpcd)

    # hostapd
    hostapd = ""
    with open("./ap-config/hostapd.conf", "r") as f:
        hostapd = f.read()
    hostapd = hostapd.replace("$SSID", ssid)
    hostapd = hostapd.replace("$PWD", password)
    os.makedirs("/etc/hostapd", exist_ok=True)
    with open("/etc/hostapd/hostapd.conf", "w") as f:
        f.write(hostapd)
    subprocess.run(["systemctl", "unmask", "hostapd"])
    subprocess.run(["systemctl", "enable", "hostapd"])

    # start hostapd; check exit status
    exit_code = subprocess.run(["systemctl", "start", "hostapd"]).returncode
    if exit_code != 0:
        raise Exception("Failed to start hostapd. Have you ran install.sh? Is the Wi-FI adapter plugged in, and does it support AP mode?")


def disable_hotspot():
    os.remove("/etc/hostapd/hostapd.conf")
    subprocess.run(["systemctl", "mask", "hostapd"])
    subprocess.run(["systemctl", "disable", "hostapd"])
    exit_code = subprocess.run(["systemctl", "stop", "hostapd"]).returncode
    if exit_code != 0:
        raise Exception("Failed to stop hostapd")


def eject_webui():
    s2 = subprocess.run(["systemctl", "disable", "webui"])
    if s2.returncode != 0:
        raise Exception("Failed to disable webui")
    
    s2 = subprocess.run(["systemctl", "stop", "webui"])
    if s2.returncode != 0:
        raise Exception("Failed to stop webui")


import socket
import subprocess
import platform


def get_hostnames_and_ips():
    self_hostnames = []

    # Get the hostname and IP of the current machine
    hostname = socket.gethostname()
    ip_address = socket.gethostbyname(hostname)
    self_hostnames.append(hostname)
    self_hostnames.append(ip_address)

    # Determine the appropriate command based on the operating system
    if platform.system().lower() == "windows":
        command = ["arp", "-a"]
    else:  # For Unix-based systems (Linux, macOS)
        command = ["arp", "-e"]

    try:
        # Run the ARP command to get network devices
        output = subprocess.check_output(command).decode("utf-8")

        # Parse the output and extract IPs
        for line in output.split("\n")[1:]:  # Skip the header line
            parts = line.split()
            if len(parts) >= 2:
                ip = parts[0]
                self_hostnames.append(ip)

    except subprocess.CalledProcessError:
        print("Error running ARP command")

    return self_hostnames
