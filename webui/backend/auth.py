import os
import hashlib


def get_sha256_hash(string):
    return hashlib.sha256(string.encode()).hexdigest()


def get_base_creds():
    BASE_CREDS = {
        "username": "admin",
        "password_sha256": get_sha256_hash("password"),
        "jwt_secret": os.urandom(128).hex(),
        "note": "Please *please* add a password to this file by appending password=<your password> to the end of the file! Upon the next file read, the password will be hashed and the plain text password will be removed.",
        "note2": "Again, please change the password ASAP! Using the default password is a security risk!",
        "note3": "Just like how you wouldn't give out your password, don't give out your JWT token! Anyone with the JWT secret can spoof JWTs and gain unauthorized access to the WebUI.",
    }
    return BASE_CREDS


def read_creds(file="creds.txt"):
    # make creds file if it doens't exist
    if not os.path.exists(file):
        write_creds(get_base_creds(), file)
        return get_base_creds()

    with open(file, "r") as f:
        lines = f.readlines()
        map = {}
        for line in lines:
            if len(line.strip()) == 0 or line.strip().startswith("#"):
                continue
            split = line.split("=", maxsplit=1)
            map[split[0]] = split[1].strip()
    if preprocess_creds(map, file):
        return read_creds(file)
    else:
        return map


def write_creds(map, file="creds.txt"):
    with open(file, "w") as f:
        for key, value in list(map.items()):
            f.write(f"{key}={value}\n")


def preprocess_creds(map, file="creds.txt"):
    if (
        len(map) == 0
        or map.get("username") is None
        or (map.get("password") is None and map["password_sha256"] is None)
        or map.get("jwt_secret") is None
    ):
        print("[info] No credentials found in creds file, adding base creds...")
        print(
            "[info] Please change the password ASAP! Using the default password is a security risk!"
        )
        print(
            "[info] You might also notice a special field named jwt_secret in the creds file. This is used to sign JWTs."
        )
        print(
            "[info] Please keep this secret and do not share it with anyone. Anyone with the JWT secret can spoof JWTs and gain unauthorized access to the WebUI."
        )
        write_creds(get_base_creds(), file)
        exit(1)
        return True

    for k, v in list(map.items()):
        if k.lower() == "password":
            print("[info] Found password in creds file, replacing with hash...")
            hash = get_sha256_hash(v)
            map["password_sha256"] = hash
            del map[k]
            write_creds(map, file)
            return True
    return False


def get_jwt_secret():
    return read_creds().get("jwt_secret")


def get_username():
    return read_creds().get("username")


def check_password(pw):
    return read_creds().get("password_sha256") == get_sha256_hash(pw)


def set_username(username):
    creds = read_creds()
    creds["username"] = username
    write_creds(creds)


def set_password(password):
    creds = read_creds()
    creds["password_sha256"] = get_sha256_hash(password)
    write_creds(creds)


def change_credentials(username, password):
    creds = read_creds()
    creds["username"] = username
    creds["password_sha256"] = get_sha256_hash(password)
    write_creds(creds)
