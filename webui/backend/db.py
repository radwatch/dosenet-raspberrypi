import sys
import argparse
import csv
from tzwhere import tzwhere
import hashlib
import os
import subprocess
import shutil
from io import StringIO

HOME = "/home/cranbari"

saved_config_path = os.path.join(HOME, "Station.csv")
data_files_path = os.path.join(HOME, "data_files")
host = "dosenet@dosenet.dhcp.lbl.gov"
tz = tzwhere.tzwhere()  # TODO: Check for memleak

PATH = os.getcwd() + "/"


class DBTool:
    def __init__(
        self,
        name,
        nickname,
        lat,
        lon,
        cpmtorem,
        display,
        devices,
        password,
        local_path=False,
        *ID,
    ):
        self.data_path = "/home/dosenet/tmp/"
        if local_path:
            self.data_path = PATH + "tmp/"
        try:
            self.ID = ID[0]
        except Exception as ex:
            print("Auto generating ID, good choice.")
        self.output = ""
        self.name = name
        self.nickname = nickname
        self.lat = lat
        self.lon = lon
        self.timezone = tz.tzNameAt(lat, lon)
        self.output += (
            "New location at ("
            + str(lat)
            + ","
            + str(lon)
            + ") in"
            + str(self.timezone)
            + " timezone"
            + "\n"
        )
        self.cpmtorem = cpmtorem
        self.cpmtousv = cpmtorem * 10
        self.display = display
        self.devices = devices
        self.password = password
        self.gitBranch = "master"
        self.needsUpdate = 0
        self.new_station = ""

        self.saveKnownHosts()
        self.retrieve_csv()

        if not ID:
            self.setID()
        self.md5hash = self.generate_hash()
        self.addDosimeterWithID()
        self.upload_csv()
        self.upload_data_files()

    def saveKnownHosts(self):
        hosts_file = os.path.expanduser("~/.ssh/known_hosts")
        self.output += f"Saving Dosenet server key to {hosts_file}\n"

        # ssh-keyscan dosenet.dhcp.lbl.gov
        known_hosts = """
# dosenet.dhcp.lbl.gov:22 SSH-2.0-OpenSSH_8.4p1 Ubuntu-6ubuntu2.1 
dosenet.dhcp.lbl.gov ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDA8UUM73ABRjrH/75NCP2ZsodOnb6xB1Mt2E07sFWEg6FKSDAkrPLa5L+bYxzgJW0wUefVsIKaAbkJ2+tV7Chq5nDzZW5VeNGEjJKN8kkX79OqLlDHOY3Xl0s7ztwLK4DBnjDDanh+cYkmedSVZ53fKZtdHdw7yGbzvdBqzV3WpJeNV3Ph41aX2Dt3N21jMuwNluUuy/403UZHCqmkhmrrTdxQkJ5YMvFv46b7fat7vR5zKoBu4U0v/LYouCbvfg+wIgAX+1uYpy3ZnqNFiDswFqncoeNUP4UcIXNB6fP9gZeA1wN7xBQeZF7gnIKUEYak6sVI9K/IDN+iKYJOy6hAU3GM6uGrukQ1AHX1IN8SmzY8Zbpa0RP1YxdmgTu1QmKbiq/+dJ/XeFidF7h9BjvPKeJSz033t0ZQyEKaNC3I+/w5MkVXAsN0KNAOOKRYI0/b77ekp/oiVhc0K8I3fUmdbH/EeBEZzxpV22qKXeq7cgLo2nmtyyB2wj4NN3wU3Ns= 
# dosenet.dhcp.lbl.gov:22 SSH-2.0-OpenSSH_8.4p1 Ubuntu-6ubuntu2.1 
dosenet.dhcp.lbl.gov ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBDheUk4u4d4suYTkgIYRJiTuWZ39aloJFRdEaBLX8J5E5pzaAnAPlpI3ll1ee9agETRhLASirrLIvj1d2AIS3qE= 
# dosenet.dhcp.lbl.gov:22 SSH-2.0-OpenSSH_8.4p1 Ubuntu-6ubuntu2.1 
dosenet.dhcp.lbl.gov ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJyCc4AHjw//lcbozhi+oiJA9CzGW4/hBJLM8Z4H56Bl 
# dosenet.dhcp.lbl.gov:22 SSH-2.0-OpenSSH_8.4p1 Ubuntu-6ubuntu2.1 
# dosenet.dhcp.lbl.gov:22 SSH-2.0-OpenSSH_8.4p1 Ubuntu-6ubuntu2.1 """
        os.makedirs(os.path.dirname(hosts_file), exist_ok=True)
        with open(hosts_file, "w") as f:
            f.write(known_hosts)

    def retrieve_csv(self):
        self.output += (
            f"Retrieving station list from DoseNet server to {saved_config_path}\n"
        )
        sourcePath = "dosenet@dosenet.dhcp.lbl.gov:~/tmp/Station.csv"
        exit_code = subprocess.check_output(
            ["sshpass", "-p", self.password, "scp", sourcePath, saved_config_path]
        )
        # if (exit_code != 0):
        #     raise Exception("CSV Retrieval Failed: scp")

    def upload_csv(self):
        destPath = "dosenet@dosenet.dhcp.lbl.gov:~/tmp/Station.csv"
        self.output += f"Uploading new station list to DoseNet server: {saved_config_path} -> {destPath}\n"
        subprocess.call(
            ["sshpass", "-p", self.password, "scp", saved_config_path, destPath]
        )
        # if (exit_code != 0):
        #    raise Exception("CSV Upload Failed")

    def upload_data_files(self):
        for data_file in os.listdir(data_files_path):
            f = os.path.join(data_files_path, data_file)
            destPath = "dosenet@dosenet.dhcp.lbl.gov:~/tmp/dosenet"
            self.output += (
                f"Uploading new data file to DoseNet server: {f} -> {destPath}\n"
            )
            subprocess.call(["sshpass", "-p", self.password, "scp", f, destPath])
            # if (exit_code != 0):
            #    raise Exception("CSV Data Upload Failed")

    def addDosimeterWithID(self):
        if not self.check_unique(self.ID, self.name):
            raise Exception(str(self.ID) + " or " + str(self.name) + " already taken")
        else:
            text = "{},{},{},{},{},{},{},{},{},{},{},{},{}\n".format(
                self.ID,
                self.name,
                self.lat,
                self.lon,
                self.cpmtorem,
                self.cpmtousv,
                self.display,
                self.devices,
                self.gitBranch,
                self.needsUpdate,
                self.md5hash,
                self.nickname,
                self.timezone,
            )
            self.runText(text)
            self.saveConfig()
            self.makeDataCSV()

    def get_keys(self):
        to_read = "~/dropbox_keys.csv"
        output = subprocess.check_output(
            [
                "sshpass",
                "-p",
                self.password,
                "ssh",
                "dosenet@dosenet.dhcp.lbl.gov",
                "cat ~/dropbox_keys.csv",
            ],
            text=True,
        )
        f = csv.DictReader(StringIO(output))
        return next(f)

    def saveConfig(self):
        fname = os.path.join(HOME, "config/config.csv")
        secrets = self.get_keys()
        self.output += f"Saving config file to {fname}\n"
        os.makedirs(os.path.dirname(fname), exist_ok=True)
        with open(fname, "w") as csvfile:
            stationwriter = csv.writer(csvfile, delimiter=",")
            stationwriter.writerow(
                [
                    "stationID",
                    "Key",
                    "Secret",
                    "Token",
                    "nickname",
                    "timezone",
                    "lat",
                    "long",
                ]
            )
            stationwriter.writerow(
                [
                    self.ID,
                    secrets["Key"],
                    secrets["Secret"],
                    secrets["Token"],
                    self.nickname,
                    self.timezone,
                    self.lat,
                    self.lon,
                ]
            )

    def setID(self):
        try:
            station_file = open(saved_config_path, "r")
            station_file.readline()
            lines = station_file.readlines()
            max_id = 0
            for line in lines:
                data = line.split(",")
                this_id = int(data[0])
                if 10000 > this_id and this_id > max_id:
                    max_id = this_id
            self.ID = max_id + 1
            self.output += f"Setting station ID to {self.ID}\n"
            station_file.close()
        except Exception as ex:
            raise ex

    def runText(self, text):
        self.output += (
            f"Writing new station to local station list at {saved_config_path}\n"
        )
        try:
            station_file = open(saved_config_path, "a+")
            station_file.write(text)
            station_file.close()
        except Exception as ex:
            raise ex

    def check_unique(self, id, name):
        try:
            station_file = open(saved_config_path, "r")
            lines = station_file.readlines()
            for line in lines:
                data = line.split(",")
                if data[0] == id:
                    return False
                if data[1] == name:
                    return False
            station_file.close()
            return True
        except Exception as ex:
            raise ex

    def getID(self, name):
        return 20001
        try:
            station_file = open(saved_config_path, "r")
            lines = station_file.readlines()
            this_id = -1
            for line in lines:
                data = line.split(",")
                if name == data[1]:
                    this_id = data[0]
                    break
            station_file.close()
            return this_id
        except Exception as ex:
            raise ex

    def generate_hash(self):
        key = "{}{}{}".format(self.ID, self.lat, self.lon)
        return hashlib.md5(key.encode()).hexdigest()

    def makeDataCSV(self):
        shutil.rmtree(data_files_path, ignore_errors=True)
        os.mkdir(data_files_path)

        sensors = ["", "_d3s", "_aq", "_adc", "_weather"]
        # meta_data = [,
        #              []
        #             ]
        for i, sensor in enumerate(sensors):
            if self.devices[i] == "1":
                # fname = "/home/pi/data_files/%s_%s.csv" % (self.nickname,sensor)
                fname = os.path.join(HOME, "data_files/%s%s.csv") % (
                    self.nickname,
                    sensor,
                )
                # fname = self.data_path + "dosenet/%s%s.csv" % (self.nickname,sensor)

                with open(fname, "w") as csvfile:
                    fwriter = csv.writer(csvfile, delimiter=",")
                    if i == 0:
                        fwriter.writerow(
                            [
                                "deviceTime_utc",
                                "deviceTime_local",
                                "deviceTime_unix",
                                "cpm",
                                "cpmError",
                                "error_flag",
                            ]
                        )
                    if i == 1:
                        meta_data = [
                            "deviceTime_utc",
                            "deviceTime_local",
                            "deviceTime_unix",
                            "cpm",
                            "cpmError",
                            "keV_per_ch",
                        ]
                        for i in range(1024):
                            meta_data.append(str(i))
                        meta_data.append("error_flag")
                        fwriter.writerow(meta_data)
                    if i == 2:
                        fwriter.writerow(
                            [
                                "deviceTime_utc",
                                "deviceTime_local",
                                "deviceTime_unix",
                                "PM1",
                                "PM25",
                                "PM10",
                                "error_flag",
                            ]
                        )
                    if i == 3:
                        fwriter.writerow(
                            [
                                "deviceTime_utc",
                                "deviceTime_local",
                                "deviceTime_unix",
                                "co2_ppm",
                                "noise",
                                "error_flag",
                            ]
                        )
                    if i == 4:
                        fwriter.writerow(
                            [
                                "deviceTime_utc",
                                "deviceTime_local",
                                "deviceTime_unix",
                                "temperature",
                                "pressure",
                                "humidity",
                                "error_flag",
                            ]
                        )
                    self.output += f"Generating data file locally at {fname}\n"
