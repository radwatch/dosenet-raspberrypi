#!/bin/bash
cd $(dirname $0)

gunicorn --workers 1 -b 0.0.0.0:80 app:app