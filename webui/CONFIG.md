## `/etc/dhcpcd.conf`

```
interface wlan1
    static ip_address=192.168.4.1/24
    nohook wpa_supplicant
```

## `/etc/dnsmasq.conf` (check ap-config)

```
interface=wlan1
dhcp-range=192.168.4.2,192.168.4.20,255.255.255.0,24h
address=/#/192.168.4.1
bind-interfaces
listen-address=192.168.4.1
except-interface=lo
```

## `/etc/hostapd/hostapd.conf`

```
country_code=US
interface=wlan1
ssid=YOURSSID
channel=9
auth_algs=1
wpa=2
wpa_passphrase=YOURPWD
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
```

## `/etc/default/hostapd`

`DAEMON_CONF="/etc/hostapd/hostapd.conf"`

## Unmasking `hostapd`

```
sudo systemctl unmask hostapd
sudo systemctl enable hostapd
sudo systemctl start hostapd
```
