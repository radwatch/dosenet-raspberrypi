# -*- coding: utf-8 -*-
"""
This module is loaded to provide the DataWriter class.
"""

from __future__ import print_function

import argparse
import time
import ast
import numpy as np
from contextlib import closing
from collections import OrderedDict

from auxiliaries import set_verbosity, Config
from globalvalues import DEFAULT_CONFIG, DATA_PATH
from globalvalues import NETWORK_LED_BLINK_PERIOD_S

from data_tools import TextObject

D3S_PREPEND_STR = '{:05d}'


class DataWriter(object):
    """

    Provides DataWriter.write_cpm(), etc. for writing data packets to local csv files prior to sending to Data Server

    """

    def __init__(self,
                 manager=None,
                 config=None,
                 verbosity=1,
                 logfile=None,
                 mode=None,
                 ):
        """
        network_status, config, publickey, aes loaded from manager
          if not provided.
        address and port take system defaults, although without config and
          publickey, address and port will not be used.
        """
        self.v = verbosity
        if manager and logfile is None:
            set_verbosity(self, logfile=manager.logfile)
        else:
            set_verbosity(self, logfile=logfile)

        self.db = TextObject(Data_Path=DATA_PATH)

        self.handle_input(
            manager, mode, config)

    def handle_input(
            self, manager, mode, config):

        # TODO: this stuff is messy. Is there a cleaner way using exceptions?
        if manager is None:
            self.vprint(1, 'DataWriter starting without Manager object')
        self.manager = manager

        try:
            if mode is None:
                self.mode = 'upload'
            elif mode == 'test':
                self.mode = 'test'
        except AttributeError:
            # if mode is not a string or None, then mode.lower() raises this
            raise RuntimeError('Invalid DataWriter mode')

        if config is None:
            if manager is None:
                self.vprint(1, 'Running DataWriter without config file')
                self.config = None
            else:
                self.config = manager.config
        else:
            self.config = config

    def construct_packet(self, sensor_type, timestamp, cpm, cpm_error, error_code=0):

        packet_dict = OrderedDict()

        if self.config is None:
            ID = 0
        else:
            ID = self.config.ID

        packet_dict['stationID'] = int(ID)
        packet_dict['deviceTime'] = float(timestamp)
        packet_dict['cpm'] = float(cpm)
        packet_dict['cpm_error'] = float(cpm_error)
        packet_dict['error_flag'] = int(error_code)

        self.vprint(3, 'Constructed packet')
        return packet_dict

    def construct_packet_D3S(self, sensor_type, timestamp, spectra, error_code=0):
        packet_dict = OrderedDict()

        if self.config is None:
            ID = 0
        else:
            ID = self.config.ID

        packet_dict['stationID'] = int(self.config.ID)
        packet_dict['deviceTime'] = float(timestamp)
        packet_dict['spectra'] = ast.literal_eval(spectra)
        packet_dict['error_flag'] = int(error_code)

        self.vprint(3, 'Constructed packet')
        return packet_dict

    def construct_packet_Env(self, sensor_type, timestamp, average_data, error_code=0):
        """
        Constructing of packet to send data for the AQ, CO2, Weather sensors
        Content of avgdata_str varies based on the type of sensor
        AQ: PM1, PM25, PM10, U03, U05, U1, U25, U5, U10
        CO2: Concentration, UV
        Weather: Temperature, Pressure, Humidity
        """
        #avgdata_str = str(average_data).replace(',', ';')

        packet_dict = OrderedDict()

        if self.config is None:
            ID = 0
        else:
            ID = self.config.ID

        packet_dict['stationID'] = int(self.config.ID)

        if sensor_type == 'AQ':
            packet_dict['deviceTime'] = float(timestamp)
            #tmp = ast.literal_eval(average_data)
            tmp = average_data
            packet_dict['oneMicron'] = tmp[0]
            packet_dict['twoPointFiveMicron'] = tmp[1]
            packet_dict['tenMicron'] = tmp[2]
            packet_dict['error_flag'] = int(error_code)

        elif sensor_type == 'co2':
            packet_dict['deviceTime'] = float(timestamp)
            #tmp = ast.literal_eval(average_data)
            tmp = average_data
            packet_dict['co2_ppm'] = tmp[0]
            packet_dict['noise'] = tmp[1]
            packet_dict['error_flag'] = int(error_code)

        elif sensor_type == 'weather':
            packet_dict['deviceTime'] = float(timestamp)
            #tmp = ast.literal_eval(average_data)
            tmp = average_data
            packet_dict['temperature'] = tmp[0]
            packet_dict['pressure'] = tmp[1]
            packet_dict['humidity'] = tmp[2]
            packet_dict['error_flag'] = int(error_code)

        self.vprint(3, 'Constructed packet')
        return packet_dict

    def construct_log_packet(self, msg_code, msg_text):
        """
        Save a message to be recorded in the server log.

        ID,"LOG",msg_code,msg_text
        """

        if not isinstance(msg_code, int):
            raise TypeError('msg_code should be an int')

        if self.config is None:
            ID = 0
        else:
            ID = self.config.ID

        packet_dict['stationID'] = int(self.config.ID)
        packet_dict['msgCode'] = msg_code
        packet_dict['msgText'] = msg_text

        self.vprint(3, 'Constructed log packet')
        return packet_dict

    def send_cpm(self, sensor_type, timestamp, cpm, cpm_error, error_code=0):
        """
        New protocol for send_cpm
        """
        packet = self.construct_packet(
            sensor_type, timestamp, cpm, cpm_error, error_code=error_code)
        self.db.insertIntoDosenet(**packet)

    def send_log(self, msg_code, msg_text):
        """
        Send a log message
        """
        packet = self.construct_log_packet(msg_code, msg_text)
        self.db.insertIntoLog(**packet)

    def send_spectra_D3S(self, sensor_type, timestamp, spectra, error_code=0):
        """
        TCP for sending spectra
        """
        packet = self.construct_packet_D3S(
            sensor_type, timestamp, spectra, error_code=error_code)
        self.db.insertIntoD3S(**packet)

    def send_data_Env(self, sensor_type, timestamp, average_data, error_code=0):
        """
        Protocol for sending data for the
        Air Quality Sensor, CO2 Sensor and Weather Sensor
        """
        try:
            packet = self.construct_packet_Env(
                sensor_type, timestamp, average_data, error_code=error_code)
        except Exception as e:
            print("ERROR constructing Env data packet!")
            print(e)
            pass
        if sensor_type == 'AQ':
            self.db.insertIntoAQ(**packet)
        elif sensor_type == 'co2':
            self.db.insertIntoCO2(**packet)
        elif sensor_type == 'weather':
            self.db.insertIntoWeather(**packet)


class PacketError(Exception):
    pass


class MissingFile(PacketError):
    pass



if __name__ == '__main__':
    # send a test log entry
    parser = argparse.ArgumentParser(
        description='Writer for saving data. ' +
        'Normally called from manager.py. ' +
        'Called directly, it will send a log message to the server.')
    parser.add_argument('--config', '-g', type=str, default=DEFAULT_CONFIG,
                        help='config file location')
    parser.add_argument('--msgcode', '-c', type=int, default=0,
                        help='message code for log')
    parser.add_argument('--message', '-m', type=str,
                        default='ServerSender test',
                        help='message text for log')
    args = parser.parse_args()
